(function() {
  var isTesting = true;
  var _testID = "1001";
  var _trackerName = "Monetate_Custom_Tracking_".concat(_testID);
  var _customDimension = 16;
  var _eventCategory = "CRO Test Data";
  var _eventActionDetail = "AWL114-Control";
  var _trackingID = "UA-76601428-1";

  // * -----------------DO NOT EDIT BELOW THIS LINE----------------- * //
  function gaSendEvent(eventAction, eventLabel, impressionEvent) {
    var trackerExists = !!ga.getByName(_trackerName) || ga("create", _trackingID, {
      "name": _trackerName
    });
    var event_object = {
      'hitType': "event",
      'eventCategory': _eventCategory,
      'eventAction': "".concat(_eventActionDetail, ": ").concat(eventAction),
      'eventLabel': eventLabel,
      'eventValue': 0,
      'nonInteraction': 1
    };
    if (impressionEvent) {
      event_object["dimension".concat(_customDimension)] = "".concat(_eventActionDetail, ": ").concat(eventAction);
    }
    if (isTesting) {
      console.log(event_object);
    }
    ga(_trackerName + ".send", event_object);
  }
  function applyEventListeners() {
    gaSendEvent('Loaded', 'Variation Loaded', true);
    $('body').on('click', '.mn-rr-placementWrapper .rr-product', function() {
      gaSendEvent('Click', 'RR Product Interaction', false);
    })
  }
  function trackingConditions() {
    return typeof ga !== "undefined";
  }
  function pollForGA() {
    var pollInterval = 5;
    var pollLimit = 10;

    var x = 0;
    var waitForLoad = function waitForLoad() {
      if (trackingConditions()) {
        applyEventListeners();
      } else if (!(typeof ga !== "undefined") && x < pollLimit) {
        x++;
        window.setTimeout(waitForLoad, pollInterval);
      }
    };
    window.setTimeout(waitForLoad, pollInterval);
  }

  window.mn_custom_ga = window.mn_custom_ga || [];
  var tracking_applied = window.mn_custom_ga[_eventActionDetail + ' ' + _trackingID];

  if (tracking_applied === true) {
    if (isTesting) {
      console.warn('Google Analytics test tracking code already applied');
    }
  } else {
    window.mn_custom_ga[_eventActionDetail + ' ' + _trackingID] = true;
    pollForGA();
  }
})()