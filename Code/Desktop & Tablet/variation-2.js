function getSessionID() {
  if (tagDataLayer && tagDataLayer.WebSessionID) {
    let session = tagDataLayer.WebSessionID.split("_")[2];
    if (session) {
      return session;
    }
  }
  return "";
}

function getUserID() {
  return tagDataLayer && tagDataLayer.AccountDetails && tagDataLayer.AccountDetails.accountRef ? tagDataLayer.AccountDetails.accountRef : "";
}

function externalJS(src, callback) {
  var s = document.createElement('script');
  s.src = src;
  s.async = true;
  s.onreadystatechange = s.onload = function () {
    var state = s.readyState;
    if (!callback.done && (!state || /loaded|complete/.test(state))) {
      callback.done = true;
      callback();
    }
  };
  document.getElementsByTagName('head')[0].appendChild(s);
}

function injectCSS(cssLink) {
  let head = document.getElementsByTagName('head')[0],
          link = document.createElement('link');
  link.rel = 'stylesheet';
  link.type = 'text/css';
  link.href = cssLink;
  head.appendChild(link);
}

function roundHalf(num) {
    return Math.round(num*2)/2;
}

function getProductRating(str) {
  var output = ''
  if (str.length !== 0) {
    var rating_figure = str.replace(/(http:\/\/reviews\.domain\.com\/1336\/)|(\/5\/rating\.gif)/g, '')
    var rating_rounded = roundHalf(rating_figure)
    var rating_text = "norating"
    switch (rating_rounded) {
      case 1:
        rating_text = "one";
        break;
      case 1.5:
        rating_text = "oneAndHalf";
        break;
      case 2:
        rating_text = "two";
        break;
      case 2.5:
        rating_text = "twoAndHalf";
        break;
      case 3:
        rating_text = "three";
        break;
      case 3.5:
        rating_text = "threeAndHalf";
        break;
      case 4:
        rating_text = "four";
        break;
      case 4.5:
        rating_text = "fourAndHalf";
        break;
      case 5:
        rating_text = "five";
        break;
    }
    var template =  `<div class="rating" itemprop="aggregateRating" itemscope="" itemtype="//schema.org/AggregateRating">
                        <strong class="score ${rating_text}">Average customer rating is ${rating_figure} out of 5 stars.</strong>
                    </div>`
        output = template
  }
  return output
}

$(function () {
  if (!$(".mn-rr-placementWrapper").length) {

    // Configurables
    const live = true, // Set to false for testing, set to true for live environment (placement must be published in RR dashboard first!)
            apiKey = "36222f2bb7365b5f", // API key obtained from RR dashboard
            placementId = "home_page.rr1", // Set placement ID from RR dashboardapiKey = '8278bb13e49dd840', // API key obtained from RR dashboard
            env = live ? 'recs' : 'integration', // Sets live/testing URL
            sessionId = getSessionID(), // User's session ID
            userId = getUserID(), // User's username
            targetNode = ".hp-section.hp-section--slot-group-1", // HP Slot to insert RR after
            name = document.querySelector('.gui-my-account-name-txt'),
            nameStr = name !== null ? `${name.textContent.trim()}, we`: "We"
            rrTitle = `${nameStr} think you'll like these`,
            placementWrapper = `<div class="mn-rr-placementWrapper">
                                          <div class="rr-copy-wrapper">
                                            <div class="rr-hl"></div>
                                            <p class="rr-title">${rrTitle}</p>
                                            <div class="rr-hl"></div>
                                          </div>
                                          <div class="mn-rr-carousel owl-carousel" id="rr-placement"></div>
                                        </div>`, // The template for which we will insert product data into
            placementElementSelector = "#rr-placement";

    $(targetNode).after(placementWrapper);

    if (typeof itemObjArray !== "undefined")
      itemObjArray = undefined;

    externalJS("//media.richrelevance.com/rrserver/js/1.2/p13n.js", function () {

      RR.jsonCallback = function () {

        const jsonPlacementInterval = window.setInterval(jsonPlacementProcessor, 1);

        function jsonPlacementProcessor() {

          if (typeof jsonPlacement === "object" && jsonPlacement.length && !$(".rr-product").length) {
            window.clearInterval(jsonPlacementInterval);

            const products = jsonPlacement[0].items;

            for (let i = 0; i < products.length; i++) {
            	let product = products[i]
            	let product_price = product.price
      				let product_sale_price = product.saleprice
      				let product_on_sale = parseFloat(product_price) !== parseFloat(product_sale_price)
      				let product_sale_price_string = `<p class="rr-price wasPrice">Was £${product_price}</p>
      													<p class="rr-price nowPrice">£${product_sale_price}</p>`
      				let product_price_string = `<p class="rr-price">£${product_sale_price}</p>`
      				let price_string = product_on_sale ? product_sale_price_string : product_price_string
      				let product_sale_badge = product_on_sale ? '<div class="sale-badge overRoundel">Sale</div>' : ''
              let product_rating = getProductRating(product.rating)
      				let product_string = `<a class="rr-product" href="${product.url}">
      										<div class="product_image_container">
      											<img alt="${product.name}" src="${product.image}">
      											${product_sale_badge}
      										</div>
      										<h4 class="rr-name">${product.name}</h4>
      										${price_string}
      									</a>`
      				$(placementElementSelector).append(product_string)
            	console.log(product)
            }
            // start carousel js
            injectCSS("https://images2.drct2u.com/content/images/ff_stylesheets/owl.carousel.min.css");
            injectCSS("https://images2.drct2u.com/content/images/ff_stylesheets/owl.theme.default.min.css");
            $.getScript('https://images2.drct2u.com/content/common/images/ejavascript/owl.carousel.min.js', function () {
              $('.mn-rr-carousel').owlCarousel({
                loop: true,
                margin: 10,
                nav: true,
                navText: ["", ""],
                responsive: {
                  0: {
                    items: 2
                  },
                  768: {
                    items: 5
                  }
                }
              });
            });
          }
        }
        
        window.setTimeout(function() {
          window.clearInterval(jsonPlacementInterval);
        }, 5000);
      };

      if (typeof RR.jsonCallback === 'function') {
        RR.jsonCallback();
      }

      try {
        processAffinities(itemObjArray);
      } catch (e) {
      }

      var R3_COMMON = new r3_common();

      R3_COMMON.setApiKey(apiKey);
      R3_COMMON.setBaseUrl(window.location.protocol + '//' + env + '.richrelevance.com/rrserver/');
      R3_COMMON.setClickthruServer(window.location.protocol + "//" + window.location.host);
      R3_COMMON.setSessionId(sessionId);
      R3_COMMON.setUserId(userId);  // if no user ID is available, please leave this blank

      // Replace placement_name with the placement name you set up in Dashboard.
      // Call multiple times to display more than one placement.
      R3_COMMON.addPlacementType(placementId);
      
      // Product seed - omitted from results, but used by Advanced Merchandising rules as a seed
      R3_COMMON.addItemId('AJ685');

      var R3_HOME = new r3_home();
      rr_flush_onload();
      r3();
      var isTesting = true;
      var _testID = "1001";
      var _trackerName = "Monetate_Custom_Tracking_".concat(_testID);
      var _customDimension = 16;
      var _eventCategory = "CRO Test Data";
      var _eventActionDetail = "AWL114-Variation-2";
      var _trackingID = "UA-76601428-1";

      // * -----------------DO NOT EDIT BELOW THIS LINE----------------- * //
      function gaSendEvent(eventAction, eventLabel, impressionEvent) {
        var trackerExists = !!ga.getByName(_trackerName) || ga("create", _trackingID, {
          "name": _trackerName
        });
        var event_object = {
          'hitType': "event",
          'eventCategory': _eventCategory,
          'eventAction': "".concat(_eventActionDetail, ": ").concat(eventAction),
          'eventLabel': eventLabel,
          'eventValue': 0,
          'nonInteraction': 1
        };
        if (impressionEvent) {
          event_object["dimension".concat(_customDimension)] = "".concat(_eventActionDetail, ": ").concat(eventAction);
        }
        if (isTesting) {
          console.log(event_object);
        }
        ga(_trackerName + ".send", event_object);
      }
      function applyEventListeners() {
        gaSendEvent('Loaded', 'Variation Loaded', true);
        $('body').on('click', '.mn-rr-placementWrapper .rr-product', function() {
            gaSendEvent('Click', 'RR Product Interaction', false);
        })
        $('body').on('click', '.mn-rr-placementWrapper .owl-prev, .mn-rr-placementWrapper .owl-next', function() {
            gaSendEvent('Click', 'RR Gallery Interaction', false);
        })
      }
      function trackingConditions() {
        return typeof ga !== "undefined";
      }
      function pollForGA() {
        var pollInterval = 5;
        var pollLimit = 10;

        var x = 0;
        var waitForLoad = function waitForLoad() {
          if (trackingConditions()) {
            applyEventListeners();
          } else if (!(typeof ga !== "undefined") && x < pollLimit) {
            x++;
            window.setTimeout(waitForLoad, pollInterval);
          }
        };
        window.setTimeout(waitForLoad, pollInterval);
      }

      window.mn_custom_ga = window.mn_custom_ga || [];
      var tracking_applied = window.mn_custom_ga[_eventActionDetail + ' ' + _trackingID];

      if (tracking_applied === true) {
        if (isTesting) {
          console.warn('Google Analytics test tracking code already applied');
        }
      } else {
        window.mn_custom_ga[_eventActionDetail + ' ' + _trackingID] = true;
        pollForGA();
      }
    });
  }
});
